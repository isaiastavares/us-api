SELECT cidades.id as id_cidade,
       estados.id as id_estado,
       cidades.nome as cidade,
	   estados.nome as estado,
       estados.uf as uf
FROM   cidades
INNER JOIN estados ON estados.id = cidades.estados_id
WHERE  cidades.nome = :cidade
AND    (estados.nome = :estado or estados.uf = :estado)