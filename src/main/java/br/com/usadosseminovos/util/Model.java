package br.com.usadosseminovos.util;

import java.util.LinkedHashMap;

public class Model extends LinkedHashMap<String, Object> {

	private static final long serialVersionUID = 1195447011858231782L;
	
	public String getStr(String key) {
		Object value = get(key);
		return value == null ? "" : value.toString();
	}
	
	public Integer getInt(String key) {
		Object value = get(key);
		
		if (value == null) {
			return 0;
		}
		
		try {
			return Integer.parseInt(value.toString());
		} catch (Exception e) {
			return 0;
		}
	}

}
