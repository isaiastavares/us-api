package br.com.usadosseminovos.localizacao.dao;

import org.springframework.stereotype.Repository;

import br.com.usadosseminovos.general.dao.AbstractDao;
import br.com.usadosseminovos.util.Model;

@Repository
public class LocalizacaoDao extends AbstractDao {
	
	public Model consultarLocalizacao(Model localizacao) {
		return query(getSql("consultar-localizacao.sql"), localizacao);
	}

}
