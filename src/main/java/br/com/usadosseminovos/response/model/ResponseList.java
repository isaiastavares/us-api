package br.com.usadosseminovos.response.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "retorno")
@JsonInclude(value = Include.NON_EMPTY)
public class ResponseList {

	@JacksonXmlElementWrapper(localName = "sucessos")
	@JacksonXmlProperty(localName = "anuncio")
	private List<Response> sucessos;
	
	@JacksonXmlElementWrapper(localName = "falhas")
	@JacksonXmlProperty(localName = "mensagem")
	private List<String> falhas;

	public ResponseList() {
		falhas = new ArrayList<>();
		sucessos = new ArrayList<>();
	}

	public List<Response> getSucessos() {
		return sucessos;
	}

	public void setSucessos(List<Response> sucessos) {
		this.sucessos = sucessos;
	}

	public List<String> getFalhas() {
		return falhas;
	}

	public void setFalhas(List<String> falhas) {
		this.falhas = falhas;
	}

	public boolean isTemErros() {
		return !falhas.isEmpty();
	}

	public boolean isTemApenasErros() {
		return !falhas.isEmpty() && sucessos.isEmpty();
	}
}
