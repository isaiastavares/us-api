package br.com.usadosseminovos.usuario.dao;

import org.springframework.stereotype.Repository;

import br.com.usadosseminovos.general.dao.AbstractDao;
import br.com.usadosseminovos.util.Model;

@Repository
public class UsuarioDao extends AbstractDao {
	
	private static final String USUARIO = "usuario/";
	
	public Model consultarUsuario(Model usuario) {
		return query(getSql(USUARIO + "select-usuario.sql"), usuario);
	}

	public void cadastrarUsuario(Model usuario) {
		query(getSql(USUARIO + "insert-usuario.sql"), usuario);
	}
	
	public void editarUsuario(Model usuario) {
		query(getSql(USUARIO + "update-usuario.sql"), usuario);
	}

	public void saveOrUpdate(Model usuario) {
		Model usuarioBanco = consultarUsuario(usuario);
		if (usuarioBanco.isEmpty()) {
			cadastrarUsuario(usuario);
		} else {
			usuario.put("id", usuarioBanco.get("id"));
			editarUsuario(usuario);
		}
	}
}
