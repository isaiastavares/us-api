package br.com.usadosseminovos.usuario.model;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "usuario")
public class Usuario {

	private static final String REGEX_TELEFONE = "^\\(?[1-9]{2}\\)?\\s?(?:[2-8]|9[1-9])[0-9]{3}\\-?[0-9]{4}$";

	@NotNull(message = "É obrigatório informar o nome")
	private String nome;

	@NotNull(message = "É obrigatório informar o email")
	@Email
	private String email;

	@NotNull(message = "É obrigatório informar a senha")
	private String senha;

	@CPF
	private String cpf;

	@CNPJ
	private String cnpj;

	@Pattern(regexp = REGEX_TELEFONE, message = "O telefone informado é inválido.")
	private String telefone;

	@NotNull(message = "É obrigatório informar o número de celular")
	@Pattern(regexp = REGEX_TELEFONE, message = "O celular informado é inválido.")
	private String celular;

	@NotNull(message = "É obrigatório informar a cidade")
	private String cidade;

	@NotNull(message = "É obrigatório informar o estado")
	private String estado;

	private String logo;

	private String website;

	private boolean news = true;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public boolean isNews() {
		return news;
	}

	public void setNews(boolean news) {
		this.news = news;
	}
}
