package br.com.usadosseminovos.usuario.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.usadosseminovos.response.model.Response;
import br.com.usadosseminovos.response.model.ResponseList;
import br.com.usadosseminovos.usuario.model.Usuario;
import br.com.usadosseminovos.usuario.service.UsuarioService;

@Controller
@RequestMapping("/usuario")
public class UsuarioController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioController.class);
	
	@Autowired
	private UsuarioService usuarioService;
	
	@RequestMapping(value = "/cadastro", method = RequestMethod.POST,
			consumes = {MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_XML_VALUE})
	@ResponseBody
    public ResponseList cadastrarUsuario(@RequestBody @Valid Usuario usuario, BindingResult result) {
		ResponseList responseList = new ResponseList();
		if (result.hasErrors()) {
			result.getAllErrors().forEach(e -> responseList.getFalhas().add(e.getDefaultMessage()));
			return responseList;
		}
		
		try {
			usuarioService.cadastrarUsuario(usuario, responseList);
			if (responseList.isTemErros()) {
				return responseList;
			}
			Response sucesso = new Response();
			sucesso.setMensagem("Usuário " + usuario.getNome() + " cadastrado com sucesso!");
			responseList.getSucessos().add(sucesso);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			String msg = String.format("Houve a seguinte falha ao cadastrar o usuário %s: %s", 
					usuario.getNome(), 
					e.getMessage());
			responseList.getFalhas().add(msg);
		}
		
		return responseList;
    }

}
