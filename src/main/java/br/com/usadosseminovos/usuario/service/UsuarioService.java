package br.com.usadosseminovos.usuario.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.usadosseminovos.anuncio.model.Login;
import br.com.usadosseminovos.localizacao.dao.LocalizacaoDao;
import br.com.usadosseminovos.response.model.ResponseList;
import br.com.usadosseminovos.usuario.dao.UsuarioDao;
import br.com.usadosseminovos.usuario.model.Usuario;
import br.com.usadosseminovos.util.Model;
import br.com.usadosseminovos.util.Models;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioDao usuarioDao;
	
	@Autowired
	private LocalizacaoDao localizacaoDao;

	public Model consultarUsuario(Login usuario) {
		Model model = Models.newModel();
		model.put("email", usuario.getEmail());
		model.put("senha", usuario.getSenha());
		
		return usuarioDao.consultarUsuario(model);
	}

	public ResponseList cadastrarUsuario(Usuario usuario, ResponseList responseList) {
		if ((usuario.getCpf() == null && usuario.getCnpj() == null) ||
				usuario.getCpf() != null && usuario.getCnpj() != null) {
			responseList.getFalhas().add("É necessário informar o CPF ou o CNPJ do usuário");
		}
		
		Model localizacao = Models.newModel();
		localizacao.put("cidade", usuario.getCidade());
		localizacao.put("estado", usuario.getEstado());
		Model localizacaoBanco = localizacaoDao.consultarLocalizacao(localizacao);
		
		if (localizacaoBanco == null || localizacaoBanco.isEmpty()) {
			responseList.getFalhas().add("Cidade ou Estado informado é inválida");
		}
		
		if (responseList.isTemErros()) {
			return responseList;
		}
		
		Model model = Models.newModel();
		model.put("nome", usuario.getNome());
		if (usuario.getCpf() != null) {
			model.put("registro", removerMascara(usuario.getCpf()));
		} else {
			model.put("registro", removerMascara(usuario.getCnpj()));
		}
		model.put("telefone", usuario.getTelefone() == null ? null : removerMascara(usuario.getTelefone()));
		model.put("celular", removerMascara(usuario.getCelular()));
		model.put("id_cidade", localizacaoBanco.getStr("id_cidade"));
		model.put("logo", usuario.getLogo());
		model.put("website", usuario.getWebsite());
		model.put("email", usuario.getEmail());
		model.put("senha", usuario.getSenha());
		model.put("data", new Date());
		model.put("news", usuario.isNews() ? "S" : "N");
		model.put("status", "A");
		
		usuarioDao.saveOrUpdate(model);
		
		return responseList;
	}
	
	private String removerMascara(String campo) {
		return campo.replaceAll("[^0-9]", "");
	}
}
