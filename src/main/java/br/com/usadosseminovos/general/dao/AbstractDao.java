package br.com.usadosseminovos.general.dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import br.com.usadosseminovos.template.FreeMarkerEngine;
import br.com.usadosseminovos.util.Model;
import br.com.usadosseminovos.util.Models;

public abstract class AbstractDao {

	@Autowired
	protected NamedParameterJdbcTemplate jdbcTemplate;
	
	@Autowired
	protected FreeMarkerEngine freeMarkerEngine;
	
	protected Model query(String query, Model params) {
		Map<String, Object> result = new HashMap<>();
		try {
			result = jdbcTemplate.queryForMap(query, params);
		} catch (EmptyResultDataAccessException e) {
			return Models.newModel();
		}
 		return result == null ? Models.newModel() : getModel(result);
	}
	
	protected String getSql(String sqlName) {
		return freeMarkerEngine.process(sqlName);
	}
	
	private Model getModel(Map<String, Object> result) {
		Model model = Models.newModel();
		result.entrySet().forEach(e -> model.put(e.getKey(), e.getValue()));
		return model;
	}
}
