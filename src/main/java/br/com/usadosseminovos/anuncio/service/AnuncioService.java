package br.com.usadosseminovos.anuncio.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.usadosseminovos.anuncio.dao.AnuncioDao;
import br.com.usadosseminovos.anuncio.model.UsadosSeminovos;
import br.com.usadosseminovos.response.model.Response;
import br.com.usadosseminovos.response.model.ResponseList;
import br.com.usadosseminovos.usuario.service.UsuarioService;
import br.com.usadosseminovos.util.Model;

@Service
public class AnuncioService {
	
	@Autowired
	private UsuarioService usuarioService;
	
	@Autowired
	private AnuncioDao anuncioDao;
	
	public ResponseList cadastrarAnuncios(UsadosSeminovos usadosSeminovos, ResponseList responseList) {
		Model usuario = usuarioService.consultarUsuario(usadosSeminovos.getUsuario());
		
		if (usuario == null || usuario.isEmpty()) {
			responseList.getFalhas().add("O usuário informado não existe");
			return responseList;
		}
		
		usadosSeminovos.getAnuncios().forEach(anuncio -> {
			try {
				anuncioDao.cadastrarAnuncio(anuncio, usuario);
				Response sucesso = new Response();
				sucesso.setTitulo(anuncio.getTitulo());
				sucesso.setMensagem("Anúncio cadastrado com sucesso!");
				responseList.getSucessos().add(sucesso);
			} catch (Exception e) {
				String msg = String.format("Houve a seguinte falha ao cadastrar o anúncio com o título %s: %s", 
						anuncio.getTitulo(), 
						e.getMessage());
				responseList.getFalhas().add(msg);
			}
		});
		
		return responseList;
	}

}
