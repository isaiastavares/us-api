package br.com.usadosseminovos.anuncio.model;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "infoGerais")
@JsonInclude(value = Include.NON_NULL)
public class InfoGerais {
	
	@NotNull(message = "O tipo do veículo é obrigatório")
	@Range(min = 1, max = 3, message = "O tipo de veículo informado é inválido")
	private Integer tpVeic;
	
	@NotNull(message = "O estilo do veículo é obrigatório")
	private String estilo;
	
	@NotNull(message = "O fabricante do veículo é obrigatório")
	private String fabricante;
	
	@NotNull(message = "O modelo do veículo é obrigatório")
	private String modelo;

	@NotNull(message = "O ano de fabricação do veículo é obrigatório")
	@Min(value = 1888, message = "O ano de fabricação informado é inválido")
	private Integer anoFabricacao;
	
	@NotNull(message = "O ano do modelo do veículo é obrigatório")
	@Min(value = 1888, message = "O ano do modelo informado é inválido")
	private Integer anoModelo;
	
	@NotNull(message = "O tipo de combustível do veículo é obrigatório")
	private String combustivel;
	
	@NotNull(message = "A cor do veículo é obrigatória")
	private String cor;
	
	@Range(min = 1, max = 7, message = "A quantidade de portas informada é inválida")
	private Integer qtdPortas;
	
	private String km;

	public Integer getTpVeic() {
		return tpVeic;
	}

	public void setTpVeic(Integer tpVeic) {
		this.tpVeic = tpVeic;
	}

	public String getEstilo() {
		return estilo;
	}

	public void setEstilo(String estilo) {
		this.estilo = estilo;
	}

	public String getFabricante() {
		return fabricante;
	}

	public void setFabricante(String fabricante) {
		this.fabricante = fabricante;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public Integer getAnoModelo() {
		return anoModelo;
	}

	public void setAnoModelo(Integer anoModelo) {
		this.anoModelo = anoModelo;
	}

	public Integer getAnoFabricacao() {
		return anoFabricacao;
	}

	public void setAnoFabricacao(Integer anoFabricacao) {
		this.anoFabricacao = anoFabricacao;
	}

	public String getCombustivel() {
		return combustivel;
	}

	public void setCombustivel(String combustivel) {
		this.combustivel = combustivel;
	}

	public String getCor() {
		return cor;
	}

	public void setCor(String cor) {
		this.cor = cor;
	}

	public Integer getQtdPortas() {
		return qtdPortas;
	}

	public void setQtdPortas(Integer qtdPortas) {
		this.qtdPortas = qtdPortas;
	}

	public String getKm() {
		return km;
	}

	public void setKm(String km) {
		this.km = km;
	}
}
