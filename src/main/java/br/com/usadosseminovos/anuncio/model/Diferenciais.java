package br.com.usadosseminovos.anuncio.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "diferenciais")
@JsonInclude(value = Include.NON_NULL)
public class Diferenciais {
	
	private boolean blindado;
	
	private boolean colecionador;
	
	private boolean copiaChave;
	
	private boolean ipvaPago;
	
	private boolean manual;
	
	private boolean unicoDono;

	public boolean getBlindado() {
		return blindado;
	}

	public void setBlindado(boolean blindado) {
		this.blindado = blindado;
	}

	public boolean getColecionador() {
		return colecionador;
	}

	public void setColecionador(boolean colecionador) {
		this.colecionador = colecionador;
	}

	public boolean getCopiaChave() {
		return copiaChave;
	}

	public void setCopiaChave(boolean copiaChave) {
		this.copiaChave = copiaChave;
	}

	public boolean getIpvaPago() {
		return ipvaPago;
	}

	public void setIpvaPago(boolean ipvaPago) {
		this.ipvaPago = ipvaPago;
	}

	public boolean getManual() {
		return manual;
	}

	public void setManual(boolean manual) {
		this.manual = manual;
	}

	public boolean getUnicoDono() {
		return unicoDono;
	}

	public void setUnicoDono(boolean unicoDono) {
		this.unicoDono = unicoDono;
	}
}
