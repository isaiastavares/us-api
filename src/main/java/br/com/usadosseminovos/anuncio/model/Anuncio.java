package br.com.usadosseminovos.anuncio.model;

import java.math.BigDecimal;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;

public class Anuncio {
	
	@NotNull(message = "O título é obrigatório")
	private String titulo;
	
	private BigDecimal valor;
	
	@NotNull(message = "As informações gerais devem ser informadas")
	@Valid
	private InfoGerais infoGerais;
	
	private Diferenciais diferenciais;
	
	private Opcionais opcionais;
	
	@Valid
	private OutrasInformacoes outrasInformacoes;
	
	@NotEmpty(message = "É necessário informar pelo menos uma imagem")
	@JacksonXmlElementWrapper(localName = "imagens", useWrapping = false)
	private List<Imagem> imagens;

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public InfoGerais getInfoGerais() {
		return infoGerais;
	}

	public void setInfoGerais(InfoGerais infoGerais) {
		this.infoGerais = infoGerais;
	}

	public Diferenciais getDiferenciais() {
		return diferenciais;
	}

	public void setDiferenciais(Diferenciais diferenciais) {
		this.diferenciais = diferenciais;
	}

	public Opcionais getOpcionais() {
		return opcionais;
	}

	public void setOpcionais(Opcionais opcionais) {
		this.opcionais = opcionais;
	}

	public OutrasInformacoes getOutrasInformacoes() {
		return outrasInformacoes;
	}

	public void setOutrasInformacoes(OutrasInformacoes outrasInformacoes) {
		this.outrasInformacoes = outrasInformacoes;
	}

	public List<Imagem> getImagens() {
		return imagens;
	}

	public void setImagens(List<Imagem> imagens) {
		this.imagens = imagens;
	}
}
