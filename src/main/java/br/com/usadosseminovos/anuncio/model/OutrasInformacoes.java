package br.com.usadosseminovos.anuncio.model;

import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "outrasInformacoes")
@JsonInclude(value = Include.NON_NULL)
public class OutrasInformacoes {

	@Size(max = 5000, message = "As observações devem conter no máximo 5000 caracteres")
	private String observacoes;
	
	@Size(max = 5000, message = "As informações adicionais devem conter no máximo 5000 caracteres")
	private String adicionais;

	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}

	public String getAdicionais() {
		return adicionais;
	}

	public void setAdicionais(String adicionais) {
		this.adicionais = adicionais;
	}
}
