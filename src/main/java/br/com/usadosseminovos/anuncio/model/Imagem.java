package br.com.usadosseminovos.anuncio.model;

public class Imagem {
	
	private String urlImagem;

	public String getUrlImagem() {
		return urlImagem;
	}

	public void setUrlImagem(String urlImagem) {
		this.urlImagem = urlImagem;
	}
}
