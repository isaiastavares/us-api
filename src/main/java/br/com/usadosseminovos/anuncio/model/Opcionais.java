package br.com.usadosseminovos.anuncio.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "opcionais")
@JsonInclude(value = Include.NON_NULL)
public class Opcionais {
	
	private boolean airBag;
	
	private boolean alarme;

	private boolean arCondicionado;
	
	private boolean arQuente;
	
	private boolean bancoMototistaAjusteAltura;
	
	private boolean bancoCouro;
	
	private boolean cdMp3Player;
	
	private boolean cambioAutomatico;
	
	private boolean cameraRe;
	
	private boolean computadorBordo;
	
	private boolean controleSomVolante;
	
	private boolean controleTracao;
	
	private boolean desembacadorTraseiro;
	
	private boolean direcaoEletrica;
	
	private boolean direcaoHidraulica;
	
	private boolean farolXenon;
	
	private boolean farolMilhaNeblina;
	
	private boolean freioABS;
	
	private boolean gps;
	
	private boolean kitMultimidia;
	
	private boolean limpadorTraseiro;
	
	private boolean lonaMaritima;
	
	private boolean pilotoAutomatico;
	
	private boolean protetorCacamba;
	
	private boolean retrovisorEletrico;
	
	private boolean rodasLigaLeve;
	
	private boolean sensorEstacionamento;
	
	private boolean tetoSolar;
	
	private boolean tracao4X4;
	
	private boolean travasEletricas;
	
	private boolean vidrosEletricos;

	public boolean getAirBag() {
		return airBag;
	}

	public void setAirBag(boolean airBag) {
		this.airBag = airBag;
	}

	public boolean getAlarme() {
		return alarme;
	}

	public void setAlarme(boolean alarme) {
		this.alarme = alarme;
	}

	public boolean getArCondicionado() {
		return arCondicionado;
	}

	public void setArCondicionado(boolean arCondicionado) {
		this.arCondicionado = arCondicionado;
	}

	public boolean getArQuente() {
		return arQuente;
	}

	public void setArQuente(boolean arQuente) {
		this.arQuente = arQuente;
	}

	public boolean getBancoMototistaAjusteAltura() {
		return bancoMototistaAjusteAltura;
	}

	public void setBancoMototistaAjusteAltura(boolean bancoMototistaAjusteAltura) {
		this.bancoMototistaAjusteAltura = bancoMototistaAjusteAltura;
	}

	public boolean getBancoCouro() {
		return bancoCouro;
	}

	public void setBancoCouro(boolean bancoCouro) {
		this.bancoCouro = bancoCouro;
	}

	public boolean getCdMp3Player() {
		return cdMp3Player;
	}

	public void setCdMp3Player(boolean cdMp3Player) {
		this.cdMp3Player = cdMp3Player;
	}

	public boolean getCambioAutomatico() {
		return cambioAutomatico;
	}

	public void setCambioAutomatico(boolean cambioAutomatico) {
		this.cambioAutomatico = cambioAutomatico;
	}

	public boolean getCameraRe() {
		return cameraRe;
	}

	public void setCameraRe(boolean cameraRe) {
		this.cameraRe = cameraRe;
	}

	public boolean getComputadorBordo() {
		return computadorBordo;
	}

	public void setComputadorBordo(boolean computadorBordo) {
		this.computadorBordo = computadorBordo;
	}

	public boolean getControleSomVolante() {
		return controleSomVolante;
	}

	public void setControleSomVolante(boolean controleSomVolante) {
		this.controleSomVolante = controleSomVolante;
	}

	public boolean getControleTracao() {
		return controleTracao;
	}

	public void setControleTracao(boolean controleTracao) {
		this.controleTracao = controleTracao;
	}

	public boolean getDesembacadorTraseiro() {
		return desembacadorTraseiro;
	}

	public void setDesembacadorTraseiro(boolean desembacadorTraseiro) {
		this.desembacadorTraseiro = desembacadorTraseiro;
	}

	public boolean getDirecaoEletrica() {
		return direcaoEletrica;
	}

	public void setDirecaoEletrica(boolean direcaoEletrica) {
		this.direcaoEletrica = direcaoEletrica;
	}

	public boolean getDirecaoHidraulica() {
		return direcaoHidraulica;
	}

	public void setDirecaoHidraulica(boolean direcaoHidraulica) {
		this.direcaoHidraulica = direcaoHidraulica;
	}

	public boolean getFarolXenon() {
		return farolXenon;
	}

	public void setFarolXenon(boolean farolXenon) {
		this.farolXenon = farolXenon;
	}

	public boolean getFarolMilhaNeblina() {
		return farolMilhaNeblina;
	}

	public void setFarolMilhaNeblina(boolean farolMilhaNeblina) {
		this.farolMilhaNeblina = farolMilhaNeblina;
	}

	public boolean getFreioABS() {
		return freioABS;
	}

	public void setFreioABS(boolean freioABS) {
		this.freioABS = freioABS;
	}

	public boolean getGps() {
		return gps;
	}

	public void setGps(boolean gps) {
		this.gps = gps;
	}

	public boolean getKitMultimidia() {
		return kitMultimidia;
	}

	public void setKitMultimidia(boolean kitMultimidia) {
		this.kitMultimidia = kitMultimidia;
	}

	public boolean getLimpadorTraseiro() {
		return limpadorTraseiro;
	}

	public void setLimpadorTraseiro(boolean limpadorTraseiro) {
		this.limpadorTraseiro = limpadorTraseiro;
	}

	public boolean getLonaMaritima() {
		return lonaMaritima;
	}

	public void setLonaMaritima(boolean lonaMaritima) {
		this.lonaMaritima = lonaMaritima;
	}

	public boolean getPilotoAutomatico() {
		return pilotoAutomatico;
	}

	public void setPilotoAutomatico(boolean pilotoAutomatico) {
		this.pilotoAutomatico = pilotoAutomatico;
	}

	public boolean getProtetorCacamba() {
		return protetorCacamba;
	}

	public void setProtetorCacamba(boolean protetorCacamba) {
		this.protetorCacamba = protetorCacamba;
	}

	public boolean getRetrovisorEletrico() {
		return retrovisorEletrico;
	}

	public void setRetrovisorEletrico(boolean retrovisorEletrico) {
		this.retrovisorEletrico = retrovisorEletrico;
	}

	public boolean getRodasLigaLeve() {
		return rodasLigaLeve;
	}

	public void setRodasLigaLeve(boolean rodasLigaLeve) {
		this.rodasLigaLeve = rodasLigaLeve;
	}

	public boolean getSensorEstacionamento() {
		return sensorEstacionamento;
	}

	public void setSensorEstacionamento(boolean sensorEstacionamento) {
		this.sensorEstacionamento = sensorEstacionamento;
	}

	public boolean getTetoSolar() {
		return tetoSolar;
	}

	public void setTetoSolar(boolean tetoSolar) {
		this.tetoSolar = tetoSolar;
	}

	public boolean getTracao4X4() {
		return tracao4X4;
	}

	public void setTracao4X4(boolean tracao4x4) {
		tracao4X4 = tracao4x4;
	}

	public boolean getTravasEletricas() {
		return travasEletricas;
	}

	public void setTravasEletricas(boolean travasEletricas) {
		this.travasEletricas = travasEletricas;
	}

	public boolean getVidrosEletricos() {
		return vidrosEletricos;
	}

	public void setVidrosEletricos(boolean vidrosEletricos) {
		this.vidrosEletricos = vidrosEletricos;
	}
}
