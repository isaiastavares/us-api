package br.com.usadosseminovos.anuncio.model;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

@JacksonXmlRootElement(localName = "usadosseminovos")
public class UsadosSeminovos {
	
	@NotNull(message = "É obrigatório informar o usuário")
	@Valid
	private Login usuario;
	
	@JacksonXmlElementWrapper(localName = "anuncios")
	@JacksonXmlProperty(localName = "anuncio")
	@NotNull(message = "É obrigatório informar pelo menos um anúncio")
	@Valid
	private List<Anuncio> anuncios;
	
	public Login getUsuario() {
		return usuario;
	}

	public void setUsuario(Login usuario) {
		this.usuario = usuario;
	}
	
	public List<Anuncio> getAnuncios() {
		return anuncios;
	}

	public void setAnuncios(List<Anuncio> anuncios) {
		this.anuncios = anuncios;
	}
}
