package br.com.usadosseminovos.anuncio.controller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import br.com.usadosseminovos.anuncio.model.Anuncio;
import br.com.usadosseminovos.anuncio.model.Diferenciais;
import br.com.usadosseminovos.anuncio.model.Imagem;
import br.com.usadosseminovos.anuncio.model.InfoGerais;
import br.com.usadosseminovos.anuncio.model.Login;
import br.com.usadosseminovos.anuncio.model.Opcionais;
import br.com.usadosseminovos.anuncio.model.OutrasInformacoes;
import br.com.usadosseminovos.anuncio.model.UsadosSeminovos;
import br.com.usadosseminovos.anuncio.service.AnuncioService;
import br.com.usadosseminovos.response.model.ResponseList;

@Controller
@RequestMapping("/anuncio")
public class AnuncioController {
	
	@Autowired
	private AnuncioService anuncioService;
	
	@RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_XML_VALUE})
    public @ResponseBody UsadosSeminovos getAnuncioExemplo() {
        UsadosSeminovos usadosSeminovos = new UsadosSeminovos();
        
        Login usuario = new Login();
        usuario.setEmail("isaiasengsoft@gmail.com");
        usuario.setSenha("7c4a8d09ca3762af61e59520943dc26494f8941b");
        
        usadosSeminovos.setUsuario(usuario);
        
        List<Anuncio> anuncios = new ArrayList<Anuncio>();
        
        Anuncio anuncio = new Anuncio();
        anuncio.setTitulo("HB20S 1.6 COMFORT PLUS 16V");
        anuncio.setValor(new BigDecimal("51.000"));
        
        InfoGerais infoGerais = new InfoGerais();
        infoGerais.setTpVeic(1);
        infoGerais.setEstilo("Sedã");
        infoGerais.setFabricante("HYUNDAI");
        infoGerais.setModelo("HB20S 1.6 COMFORT PLUS 16V");
        infoGerais.setAnoFabricacao(2016);
        infoGerais.setAnoModelo(2016);
        infoGerais.setCombustivel("Á/G");
        infoGerais.setCor("Preto");
        infoGerais.setKm("11000");
        infoGerais.setQtdPortas(4);
        anuncio.setInfoGerais(infoGerais);
        
        Diferenciais diferenciais = new Diferenciais();
        diferenciais.setCopiaChave(true);
        diferenciais.setIpvaPago(true);
        diferenciais.setManual(true);
        diferenciais.setUnicoDono(true);
        anuncio.setDiferenciais(diferenciais);
        
        Opcionais opcionais = new Opcionais();
        opcionais.setAirBag(true);
        opcionais.setAlarme(true);
        opcionais.setArCondicionado(true);
        opcionais.setBancoMototistaAjusteAltura(true);
        opcionais.setCambioAutomatico(true);
        opcionais.setComputadorBordo(true);
        opcionais.setControleSomVolante(true);
        opcionais.setDesembacadorTraseiro(true);
        opcionais.setDirecaoEletrica(true);
        opcionais.setFarolMilhaNeblina(true);
        opcionais.setFreioABS(true);
        opcionais.setPilotoAutomatico(true);
        opcionais.setRetrovisorEletrico(true);
        opcionais.setTravasEletricas(true);
        opcionais.setVidrosEletricos(true);
        anuncio.setOpcionais(opcionais);
        
        OutrasInformacoes outrasInformacoes = new OutrasInformacoes();
        outrasInformacoes.setObservacoes("Computador de Bordo, Controle de Som no Volante, Cópia da Chave, IPVA Pago, Manual, Pneus Novos, Sem Retoque, Único dono, ");
        anuncio.setOutrasInformacoes(outrasInformacoes);
        
        List<Imagem> imagens = new ArrayList<>();
        Imagem imagem = new Imagem();
        imagem.setUrlImagem("http://usadosbr.com/cache/media/gallery/e5/d4/78/img-2859-189-image-760x570-crop.JPG");
        imagens.add(imagem);
        
        anuncio.setImagens(imagens);
        
        anuncios.add(anuncio);
        usadosSeminovos.setAnuncios(anuncios);
        
        return usadosSeminovos;
    }

	@RequestMapping(value = "/cadastro", method = RequestMethod.POST,
			consumes = {MediaType.APPLICATION_XML_VALUE},
			produces = {MediaType.APPLICATION_XML_VALUE})
	@ResponseBody
    public ResponseList cadastrarAnuncio(@RequestBody @Valid UsadosSeminovos usadosSeminovos, BindingResult result) {
		ResponseList responseList = new ResponseList();
		if (result.hasErrors()) {
			result.getAllErrors().forEach(e -> responseList.getFalhas().add(e.getDefaultMessage()));
			return responseList;
		}
		
		return anuncioService.cadastrarAnuncios(usadosSeminovos, responseList);
    }
}
