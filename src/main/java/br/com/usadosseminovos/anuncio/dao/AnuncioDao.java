package br.com.usadosseminovos.anuncio.dao;

import org.springframework.stereotype.Repository;

import br.com.usadosseminovos.anuncio.model.Anuncio;
import br.com.usadosseminovos.general.dao.AbstractDao;
import br.com.usadosseminovos.util.Model;

@Repository
public class AnuncioDao extends AbstractDao {

	public void cadastrarAnuncio(Anuncio anuncio, Model usuario) {
		query(getSql("cadastrar-anuncio.sql"), usuario);
	}
}
