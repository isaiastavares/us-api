package br.com.usadosseminovos.template;

import java.io.IOException;
import java.io.StringWriter;

import org.springframework.stereotype.Component;

import br.com.usadosseminovos.util.Model;
import br.com.usadosseminovos.util.Models;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.Version;

@Component
public class FreeMarkerEngine {
	
	private static final String UTF_8 = "UTF-8";
	private static final Version VERSION = new Version(2, 3, 20);

	private Configuration configuration = new Configuration(VERSION);

	public FreeMarkerEngine() {
		configuration.setClassForTemplateLoading(getClass(), "/sql/");
		configuration.setObjectWrapper(new DefaultObjectWrapper(VERSION));
		configuration.setDefaultEncoding(UTF_8);
		configuration.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		configuration.setIncompatibleImprovements(VERSION);
	}

	public String process(String templateName) {
		try {
			Template template = configuration.getTemplate(templateName);
			StringWriter stringWriter = new StringWriter();
			Model model = Models.newModel();
			template.process(model, stringWriter);

			return stringWriter.toString();
		} catch (IOException | TemplateException e) {
			throw new RuntimeException(e);
		}
	}
}
